<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>list Student</title>
</head>

<?php
    // $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
    
    // if (isset($_POST['select_khoa'])) {
    //     $_SESSION['select_khoa'] = $_POST['select_khoa'];
    // }
    // if (isset($_POST['key'])){
    //     $_SESSION['key'] = $_POST['key'];
    // }
?>

<style>
    /* .item_table {
        width: 50%;
        margin: 10px 20px 20px 20px;
        border-spacing: 15px;
    } */
    table, th, td{
        border: 1px solid rgb(52, 52, 206);
    }

    table {
        border-collapse: collapse;
        margin: 30px auto;
    }

    td,th {
        padding: 10px;
    }
    td {
        height: 50px;
    }
    img {
        max-width: 50px;
        max-height: 50px
    }

</style>




<body>
    <?php
        /*Cố gắng kết nối máy chủ MySQL. Giả sử bạn đang chạy MySQL
        Máy chủ có cài đặt mặc định (user là 'root' và không có mật khẩu)*/
        $link = mysqli_connect("localhost", "root", "", "student");
        // Kểm tra kết nối
        if($link === false){
            die("ERROR: Không thể kết nối. " . mysqli_connect_error());
        }
        
        // Cố gắng thực hiện câu lệnh SELECT
        $sql = "SELECT * FROM student";
        if($result = mysqli_query($link, $sql)){
            if(mysqli_num_rows($result) > 0){
                echo "<table>";
                    echo "<tr>";
                        echo "<th>id</th>";
                        echo "<th>Name</th>";
                        echo "<th>Gender</th>";
                        echo "<th>Faculty</th>";
                        echo "<th>Birthday</th>";
                        echo "<th>Address</th>";
                        echo "<th>Avartar</th>";
                    echo "</tr>";
                while($row = mysqli_fetch_array($result)){
                    echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['name'] . "</td>";
                        echo "<td>" . $row['gender'] . "</td>";
                        echo "<td>" . $row['faculty'] . "</td>";
                        echo "<td>" . $row['birthday'] . "</td>";
                        echo "<td>" . $row['address'] . "</td>";
                        $image = $row['avartar'];
                        echo "<td><img src='$image'</td>";
                    echo "</tr>";
                }
                echo "</table>";
                // Giải phóng bộ nhớ của biến
                mysqli_free_result($result);
            } else{
                echo "Không có bản ghi nào được tìm thấy.";
            }
        } else{
            echo "ERROR: Không thể thực thi câu lệnh $sql. " . mysqli_error($link);
        }
        // Đóng kết nối
        mysqli_close($link);
    ?>
</body>
</html>