<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirm</title>
    <link rel="stylesheet" href="edit.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
</head>

<body>
    
<form action="" onsubmit="return validate()" method="POST">

<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Something posted

        $error = array(); // Chưa có lỗi
        
        if (isset($_POST['submit'])) {
            $servername = "localhost";
            $username = "root";
            $password = "";

            // tạo kết nối
            $conn = mysqli_connect($servername, $username, $password);
            
            // kiểm tra kết nối
            if ($conn->connect_error) {
            die("Connection failed: " .$conn->connect_error);
            }
            // tạo cơ sở dữ liệu
            $sql = "CREATE DATABASE IF NOT EXISTS student";
            if ($conn->query($sql) === TRUE) {
            // echo "Database đã được tạo thành công";
            } else {
            $error['data']= "Tạo database chưa thành công " . $conn->error;
            
            }
            $conn->close();

            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "student";
            // tạo kết nối 
            $conn = new mysqli($servername, $username, $password, $dbname);
            // kiểm tra kết nối
            if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
            }

            //tạo bảng studet trong database student
            $sql = "CREATE TABLE IF NOT EXISTS student (
                id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(250) NOT NULL,
                gender INT(1) NOT NULL,
                faculty CHAR(3) NOT NULL,
                birthday DATETIME NOT NULL,
                address VARCHAR(250) DEFAULT NULL,
                avartar TEXT DEFAULT NULL)
                ENGINE = INNODB DEFAULT CHARSET = utf8";
            
            // kiểm tra taoh bảng
            if ($conn->query($sql) === TRUE) {
            // echo "bảng đã được tạo thành công";
            } else {
            $error['table']= "tạo bảng chưa thành công " . $conn->error;
            }

            $name = $_SESSION['fullname'];
            $gender = $_SESSION['gender1'];
            if ($gender =='Nữ'){
                $gender = 0;
            }
            if ($gender == 'Nam'){
                $gender = 1;
                
            }
            
            $faculty = $_SESSION['select_khoa'];
            if ($faculty == 'Khoa học máy tính'){
                $faculty = 'MTA';
            } else {
                $faculty = "KDL";
            }
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $birthday = $_SESSION['birthday'];
            $d = strtotime("$birthday");
            $date = "".date("Y/m/d",$d);
            echo $date;
            $address = $_SESSION['address'];
            $avartar = $_SESSION['fileupload'];

            $sql = "INSERT INTO student (name, gender, faculty, birthday, address, avartar)
            VALUES ('$name', '$gender', '$faculty','$date', '$address', '$avartar')";
        

            //echo $birthday;
            if ($conn->query($sql) === TRUE) {
                // echo "Thêm dữ liệu thành công";
            } else {
                $error['eror']  = "Error: " . $sql . "<br>" . $conn->error;
            }
            $conn->close();   
        }
        if (empty($error)) {
            header("Location: result.php");             
                
        }

    }
?>


    <div class="item_header item_header1">
        
        <!-- Hiển thị lỗi  -->
        <div class="header">

            <!-- div chọn họ và tên -->
            <div class="item">

                <div class="item_left">
                    Họ và tên
                </div>

                <div class="item_right_1">
                    <?php echo $_SESSION['fullname'];?>

                </div>
                
            </div>

            <!--  div chọn giới tính  -->
            <div class="item">

                <div class="item_left">
                    Giới tính
                </div>

                <div class="item_right_1">
                    <?php echo $_SESSION['gender1'];?>
                    
                </div>
                <!-- dùng mang đe luu thông tin giới tính, dùng for đê hien thị thông tin giới tính  --> 
            </div>


            <!--  div phân khoa -->
            <div class="item">

                <div class="item_left">
                    Phân khoa
                </div>

                <div class="item_right_1">
                    <?php echo $_SESSION['select_khoa'];?>
                </div>
                
            </div>

            <!-- div chọn ngày sinh -->
            <div class="item">

                <div class="item_left">
                    Ngày sinh
                </div>

                <div class="item_right_1">
                    <?php echo $_SESSION['birthday'];?>
                </div>
            </div>


            <!-- div chọn địa chỉ -->
            <div class="item">

                <div class="item_left">
                    Địa chỉ
                </div>

                <div class="item_right_1">
                    <?php echo $_SESSION['address'];?>
                </div>

            </div>

            <div class="item">

                <div class="item_left">
                    Hình ảnh
                </div>
                <div class="image">
                    <img class="image_1" src="
                    <?php echo $_SESSION['fileupload'];?> 
                    " alt=""> 

                </div>
                
                
            </div>

            <!-- button đăng ký -->
            <button class="button_submit submit_1" name="submit" >
                Xác Thực
            </button>
        </div>



    </div>
<!--  -->



<!--  -->


</form>
</body>
</html>